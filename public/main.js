// IP DEL SERVIDOR
const ip = '192.168.0.5'
// PUERTO POR EL QUE CORRE LA APLICACIÓN
const port = 8080

let socket = io.connect('http://'+ip+':'+port, {'forceNew':true})
let client =  '';


socket.on('messages', (data)=>{
    client = data.client;
    $('#idClient').text(client)
    render(data.messages)
})

render = (data) => {
    var html = data.map(function(elem, index) {
      return(`<div>
                <strong>${elem.client}</strong>:
                <em>${elem.text}</em>
              </div>`);
    }).join(" ");
    document.getElementById('messages').innerHTML = html;
}


$('#btnSend').click(()=>{
    if ($('#text').val()!=''){
        let message = {
            text: $('#text').val(),
            client: client
          };
          socket.emit('new-message', message);
        $('#text').val('')
    }
    return false;
})