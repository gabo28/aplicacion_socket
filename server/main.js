var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

app.use(express.static('public'))

let messages = [];

io.sockets.on('connection', function (socket) {
    let conn = socket.conn

    socket.on('disconnect', function (reason) {
      console.log('Client: ' +conn.id+' disconnect' );
    });

    socket.emit('messages', {
        client:socket.conn.id,
        messages:messages
    })

    console.log('Connection from ' + conn.remoteAddress + '   id assigned ' + conn.id);
    console.log('')

    socket.on('new-message', function(data) {
        
        messages.push(data);
        io.sockets.emit('messages', {
            client:socket.conn.id,
            messages:messages
        })
      });

})


server.listen(8080, function() {
})