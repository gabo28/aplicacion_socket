# README #

Este repositori es para temas de estudio.

### Requisitos para correr aplicación ###


* [Node.js](https://nodejs.org/es/)

### Pasos para correr la aplicacion ###

* Lo primero es clonar el repositorio y ubicarse en la raiz del proyecto

* luego de esto se debe intalr las dependencias con el comando ```npm install ```

* luego para arrancar la aplicacion copiamos el comando ```npm start```

* si se desea correr la aplicación con pm2 es necesario instalr pm2 en un entorno global ``` npm install -g pm2```

* ```pm2 start server/main.js --watch``` para iniciar la aplicación y ```pm2 monit``` para monitorial la aplicación



### Autor ###

* Gabriel Jaime Marin Isaza
